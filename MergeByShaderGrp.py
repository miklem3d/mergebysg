
from maya import cmds as cmds

"""
import sys
sys.path.append("E:\GoogleDrive\Dev\Code\merge_by_sg")

from maya import cmds as cmds
import MBSG as mbsg

merge_by_sg=mbsg.MergeByShaderGroup()
merge_by_sg.run()
"""


class MergeByShaderGroup(object):
    SELECTION = None  # store all selected items
    SELECTION_SG = None  # store all  selected items

    def __init__(self):
        pass

    def get_selection(self):
        self.SELECTION = cmds.ls(dag=True, s=True, o=True, selection=True)
        if self.SELECTION:
            self.SELECTION_SG = list(set(cmds.listConnections(self.SELECTION, type="shadingEngine")))
            if self.SELECTION_SG:
                self.merge_items()
            else:
                self.error("no_SG")
        else:
            self.error("no_selection")

    def merge_items(self):
        for sg in self.SELECTION_SG:
            cmds.select(clear=True)
            components_with_material = cmds.sets(sg, q=True)
            filtered = []
            for i in components_with_material:
                filtered.append(i.split(".")[0])
            cmds.select(filtered)
            objects = cmds.ls(dag=True, s=True, o=True, selection=True)
            filtered_objects = [x for x in objects if x in self.SELECTION]
            if len(filtered_objects) >= 2:
                cmds.polyUnite(filtered_objects, ch=False, name="merged")

    staticmethod
    def error(self, code):
        if code == "no_selection":
            cmds.confirmDialog(message="Nothing selected", button="ok")
        if code == "no_SG":
            cmds.confirmDialog(message="There is no SG on selected objects", button="ok")

    def run(self):
        self.get_selection()

